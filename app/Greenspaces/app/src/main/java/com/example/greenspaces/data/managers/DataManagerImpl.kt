package com.example.greenspaces.data.managers

import android.app.Application
import androidx.room.Database
import com.example.greenspaces.arch.DatabaseModule
import com.example.greenspaces.data.database.dao.PersonDao
import com.example.greenspaces.data.models.Person
import com.google.gson.Gson
import io.reactivex.Single
import kotlin.concurrent.thread

class DataManagerImpl(private val context: Application,
                      private val gson: Gson,
                      private val personDao: PersonDao) : DataManager {

    override fun getPerson(id: Int): Single<Person> {
        return Single.create<Person>{
            personDao.getPerson(id)
        }
    }

    override fun insert(person: Person) {
        thread {
            DatabaseModule.personDb.insertPerson(person)
        }
    }
}