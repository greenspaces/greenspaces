package com.example.greenspaces.ui.navigation.submit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.location.LocationManager
import android.os.Bundle
import android.text.TextUtils
import android.util.AndroidException
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import com.example.greenspaces.R
import com.example.greenspaces.arch.android.BaseFragment
import com.example.greenspaces.arch.hideKeyboard
import com.example.greenspaces.data.models.Submit
import com.example.greenspaces.ui.navigation.ward.WardFragment
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_submit.*
import java.util.concurrent.TimeUnit

class SubmitFragment : BaseFragment<SubmitMvp.Presenter>(), SubmitMvp.View {

    lateinit var locationManager: LocationManager
    var lat: Double = 0.0
    var lon: Double = 0.0

    companion object{
        fun newInstance(): SubmitFragment {
            return SubmitFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_submit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = SubmitPresenter(this)
        presenter.load()
    }

    fun createLocationRequest() {
        val locationRequest = LocationRequest.create()?.apply {
            interval = 30000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest!!)
        builder.setAlwaysShow(true)
        val task = LocationServices
            .getSettingsClient(context!!)
            .checkLocationSettings(builder.build())
            .addOnCompleteListener { locationSettingsResponse ->
                locationManager = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                presenter.getLocation(locationManager)
            }.addOnFailureListener { exception ->
                if (exception is ResolvableApiException){
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        //exception.startResolutionForResult(activity!!.parent, REQUEST_CHECK_SETTINGS)
                    } catch (sendEx: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }
                }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val states = LocationSettingsStates.fromIntent(data)
        when(requestCode){
            101 ->
                when(resultCode){
                    Activity.RESULT_OK ->
                        Toast.makeText(context, states.isLocationPresent.toString(), Toast.LENGTH_SHORT).show()
                    Activity.RESULT_CANCELED ->
                        Toast.makeText(context, "cancelled", Toast.LENGTH_SHORT).show()
                }
        }

    }


    override fun setupForm() {
        additional_information.setOnEditorActionListener { textView, i, keyEvent ->
            if(i == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard()
                additional_information.clearFocus()
            }
            false
        }
        height.setOnEditorActionListener { textView, i, keyEvent ->
            if(i == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard()
                height.clearFocus()
            }
            false
        }
        age.setOnEditorActionListener { textView, i, keyEvent ->
            if(i == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard()
                age.clearFocus()
            }
            false
        }
        race.setOnEditorActionListener { textView, i, keyEvent ->
            if(i == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard()
                race.clearFocus()
            }
            false
        }
        description.setOnEditorActionListener { textView, i, keyEvent ->
            if(i == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard()
                description.clearFocus()
            }
            false
        }
    }

    override fun setClickables() {
        location_button.setOnClickListener {
            coords.visibility = View.GONE
            done.visibility = View.GONE
            createLocationRequest()
        }
        submit_button.setOnClickListener {
            if(checkRequired()){
                text_error.visibility = View.GONE
                presenter.submitForm(Submit(
                    lat,
                    lon,
                    height.text.toString(),
                    age.text.toString(),
                    race.text.toString(),
                    description.text.toString(),
                    additional_information.text.toString()
                ))
                startActivity(Intent(activity, SubmittedActivity::class.java))
                activity!!.finish()
            }
            else {
                text_error.visibility = View.VISIBLE
                Observable.timer(50, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                    .map { scrollview.fullScroll(View.FOCUS_DOWN) }
                    .subscribeOn(Schedulers.io())
                    .subscribe()
            }

        }
    }

    override fun onLocationReceived(lat: Double, lon: Double) {
        this.lat = lat
        this.lon = lon
        latitude.text = context!!.getString(R.string.latitude, String.format("%.1f", lat))
        longitude.text = context!!.getString(R.string.longitude, String.format("%.1f", lon))
        coords.visibility = View.VISIBLE
        error.visibility = View.GONE
        done.visibility = View.VISIBLE
    }

    override fun finish() {

    }

    fun checkRequired() : Boolean {
        var map = mutableMapOf<View, Boolean>(
            latitude to true,
            height to true,
            age to true,
            race to true,
            description to true)

        if(lat == 0.0 || lon == 0.0) {
            error.visibility = View.VISIBLE
            map[latitude] = false
        }
        if(TextUtils.isEmpty(height.text)) {
            height.error = "height is required"
            map[height] = false
        }
        if(TextUtils.isEmpty(age.text)) {
            age.error = "age is required"
            map[age] = false
        }
        if(TextUtils.isEmpty(race.text)) {
            race.error = "race is required"
            map[race] = false
        }
        if(TextUtils.isEmpty(description.text)) {
            description.error = "description is required"
            map[description] = false
        }
        for (bool in map.values){ if(!bool) return false }
        return true
    }
}