package com.example.greenspaces.ui.navigation.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.greenspaces.R

class PlaceholderAdapter : RecyclerView.Adapter<PlaceholderAdapter.ViewHolder>(){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.element_placeholder, parent, false))
    }

    override fun getItemCount(): Int {
        return 2
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}