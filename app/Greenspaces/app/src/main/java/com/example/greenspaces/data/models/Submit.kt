package com.example.greenspaces.data.models

import java.io.Serializable

data class Submit(
    val latitude: Double,
    val longitude: Double,
    val height: String,
    val age: String,
    val race: String,
    val description: String,
    val additionalInfo: String?
):Serializable{
    companion object{
        fun fromTemplate(template: SubmitTemplate):Submit?{
            return Submit(
                template.latitude,
                template.longitude,
                template.height,
                template.age,
                template.race,
                template.description,
                template.additionalInfo.orEmpty()
            )
        }
    }
}

data class SubmitTemplate(
    val latitude: Double,
    val longitude: Double,
    val height: String,
    val age: String,
    val race: String,
    val description: String,
    val additionalInfo: String?
)