package com.example.greenspaces.ui.navigation.home

import com.example.greenspaces.arch.android.BaseMvp
import com.example.greenspaces.arch.android.BasePresenter
import com.example.greenspaces.data.models.Stat

interface HomeMvp : BaseMvp {
    interface View : BaseMvp.View {
        fun showFullCityStat(stats: List<Stat>)
        fun showError()
    }

    interface Presenter : BaseMvp.Presenter{
        fun load()
    }
}