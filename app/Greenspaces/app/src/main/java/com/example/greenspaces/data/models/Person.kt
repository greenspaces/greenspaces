package com.example.greenspaces.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.example.greenspaces.data.database.dao.*
import com.example.greenspaces.data.database.dao.ID_PERSON
import com.example.greenspaces.data.database.dao.PERSON_LATITUDE
import com.example.greenspaces.data.database.dao.PERSON_LONGITUDE
import com.example.greenspaces.data.database.dao.PERSON_TABLE_NAME
import com.google.gson.annotations.SerializedName

@Entity(tableName = PERSON_TABLE_NAME, indices = [(Index(ID_PERSON, unique = true))])

class Person {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = ID_PERSON)
    var idPerson: Int = -1

    @ColumnInfo(name = PERSON_LATITUDE)
    @SerializedName("latitude") var latitude: Double = -1.0

    @ColumnInfo(name = PERSON_LONGITUDE)
    @SerializedName("longitude") var longitude: Double = -1.0

    @ColumnInfo(name = PERSON_HEIGHT)
    @SerializedName("height") var height: String? = ""

    @ColumnInfo(name = PERSON_AGE)
    @SerializedName("age") var age: String? = ""

    @ColumnInfo(name = PERSON_RACE)
    @SerializedName("race") var race: String? = ""

    @ColumnInfo(name = PERSON_DESCRIPTION)
    @SerializedName("description") var description: String? = ""

    @ColumnInfo(name = PERSON_ADDITIONAL_INFO)
    @SerializedName("additional_info") var additionalInfo: String? = ""

    fun setPerson(idPerson: Int, latitude: Double, longitude: Double, height: String?, age: String?, race: String?, description: String?, additionalInfo: String?): Person{
        this.idPerson = idPerson
        this.latitude = latitude
        this.longitude = longitude
        this.height = height
        this.age = age
        this.race = race
        this.description = description
        this.additionalInfo = additionalInfo
        return this
    }
}
