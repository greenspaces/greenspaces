package com.example.greenspaces.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.greenspaces.data.models.Person
import io.reactivex.Flowable

internal const val PERSON_TABLE_NAME = "person"
internal const val ID_PERSON = "_id_person"
internal const val PERSON_LATITUDE = "latitude"
internal const val PERSON_LONGITUDE = "longitude"
internal const val PERSON_HEIGHT = "height"
internal const val PERSON_AGE = "age"
internal const val PERSON_RACE = "race"
internal const val PERSON_DESCRIPTION = "description"
internal const val PERSON_ADDITIONAL_INFO = "additional_information"

@Dao
interface PersonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPerson(vararg person: Person)

    @Query("SELECT * FROM $PERSON_TABLE_NAME WHERE $ID_PERSON = :id")
    fun getPerson(id: Int): Person
}