package com.example.greenspaces

import android.app.Application
import com.example.greenspaces.arch.AppModule
import net.danlew.android.joda.JodaTimeAndroid

class GreenspacesApplication : Application(){
    override fun onCreate() {
        AppModule.application = this
        super.onCreate()
        JodaTimeAndroid.init(this)
    }
}