package com.example.greenspaces.arch.android

import androidx.appcompat.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable

abstract class BaseActivity<T: BaseMvp.Presenter>: AppCompatActivity() {
    lateinit var presenter: T
    protected val subscription = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
        subscription.clear()
    }
}