package com.example.greenspaces.data.managers

import com.example.greenspaces.data.models.Stat
import io.reactivex.Single

interface StatManager {

    fun getStats(): Single<List<Stat>>
    fun getWardStats(): Single<List<Stat>>
}