package com.example.greenspaces.ui.navigation.submit

import android.content.Intent
import android.location.LocationListener
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.greenspaces.R
import com.example.greenspaces.arch.android.BaseActivity
import com.example.greenspaces.arch.android.BaseFragment
import com.example.greenspaces.ui.main.MainActivity
import com.example.greenspaces.ui.navigation.home.HomeFragment
import com.example.greenspaces.ui.navigation.submit.SubmitFragment
import com.example.greenspaces.ui.navigation.ward.WardFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_submitted.*

class SubmittedActivity : BaseActivity<SubmittedMvp.Presenter>(), SubmittedMvp.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_submitted)
        presenter = SubmittedPresenter(this)
        presenter.load()
    }

    override fun setClickables() {
        submitted_close.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}
