package com.example.greenspaces.ui.navigation.submit

import android.content.Context
import android.location.LocationManager
import com.example.greenspaces.arch.android.BaseMvp
import com.example.greenspaces.arch.android.BasePresenter
import com.example.greenspaces.data.models.Stat
import com.example.greenspaces.data.models.Submit

interface SubmitMvp : BaseMvp {
    interface View : BaseMvp.View {
        fun setupForm()
        fun setClickables()
        fun onLocationReceived(lat: Double, lon: Double)
    }

    interface Presenter : BaseMvp.Presenter{
        fun load()
        fun getLocation(l: LocationManager)
        fun submitForm(s: Submit)
    }
}