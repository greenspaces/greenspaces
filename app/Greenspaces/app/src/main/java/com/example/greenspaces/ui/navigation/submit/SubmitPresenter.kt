package com.example.greenspaces.ui.navigation.submit

import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.util.Log
import com.example.greenspaces.arch.PreferenceModule
import com.example.greenspaces.arch.android.BasePresenter
import com.example.greenspaces.data.DataModule
import com.example.greenspaces.data.managers.DataManager
import com.example.greenspaces.data.managers.StatManager
import com.example.greenspaces.data.models.Person
import com.example.greenspaces.data.models.Submit
import com.f2prateek.rx.preferences2.Preference
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*
import kotlin.random.Random

class SubmitPresenter(welcomeView: SubmitMvp.View,
                      private val dataManager: DataManager = DataModule.dataManager,
                      private val dbId: Preference<Int> = PreferenceModule.dbId) : BasePresenter<SubmitMvp.View>(welcomeView), SubmitMvp.Presenter {

    override fun load(){
        view?.setupForm()
        view?.setClickables()
    }

    override fun getLocation(l: LocationManager) {
        val location = location(l)
        location.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe ({ loc ->
                view?.onLocationReceived(loc!!.latitude, loc.longitude)
            }, { e -> Log.e("error", "error loading locaiton", e)})
        //view?.onLocationReceived(location!!.latitude, location.longitude)
    }

    fun location(l: LocationManager) : Single<Location> {
        return Single.create<Location> {
            it.onSuccess(l.getLastKnownLocation(LocationManager.GPS_PROVIDER))
        }
    }

    override fun submitForm(s: Submit) {
        var p = Person()
        p.setPerson(
            dbId.get(),
            s.latitude,
            s.longitude,
            s.height,
            s.age,
            s.race,
            s.description,
            s.additionalInfo
        )
        dataManager.insert(p)
        dataManager.getPerson(dbId.get())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d(it.description!!)
                }, { e -> Timber.e(e, "something went wrong getting person")}
        )
        dbId.set(dbId.get()+1)
    }

}