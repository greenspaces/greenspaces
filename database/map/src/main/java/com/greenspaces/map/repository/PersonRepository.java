package com.greenspaces.map.repository;

import com.greenspaces.map.model.Person;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonRepository extends CrudRepository<Person, Long> {

}
