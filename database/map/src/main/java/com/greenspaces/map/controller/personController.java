package com.greenspaces.map.controller;

import com.greenspaces.map.model.Person;
import com.greenspaces.map.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
public class personController {
    @Autowired
    private PersonRepository personRepository;

    @RequestMapping(value = "/people", method = RequestMethod.POST)
    public @ResponseBody
    Person createPerson(@RequestParam(value = "latitude", required = true) Double latitude,
                        @RequestParam(value = "longitude", required = true) Double longitude,
                        @RequestParam(value = "age", required = true) Integer height,
                        @RequestParam(value = "age", required = true) Integer age,
                        @RequestParam(value = "date", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {

        Person person = new Person(latitude, longitude, height, age, date);
        personRepository.save(person);
        return person;
    }

    @RequestMapping(value = "/people/{personId}", method = RequestMethod.GET)
    public Person getPerson(@PathVariable("personId") Long personId) {
        System.out.println(personId);
        /* validate person Id parameter */
        /*if (null==personId) {
            throw new InvalidCustomerRequestException();
        }*/
        Person person = personRepository.findById(personId).get();
        /*if(null==person){
            throw new CustomerNotFoundException();
        }*/
        return person;

    }
    @RequestMapping(value = "/people", method = RequestMethod.GET)
    public List<Person> getPeople() {
        return (List<Person>) personRepository.findAll();
    }

    @RequestMapping(value = "/people/{personId}", method = RequestMethod.DELETE)
    public void removeCustomer(@PathVariable("personId") Long customerId, HttpServletResponse httpResponse) {

        if(personRepository.existsById(customerId)){
            Person person = personRepository.findById(customerId).get();
            personRepository.delete(person);
        }
        httpResponse.setStatus(HttpStatus.NO_CONTENT.value());
    }
}
