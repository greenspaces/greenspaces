function metersToPixelsAtMaxZoom(meters, latitude){
    return meters / 0.075 / Math.cos(latitude * Math.PI / 180)
}

function loadFile(filePath) {
    var result = null;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", filePath, false);
    xmlhttp.send();
    if (xmlhttp.status==200) {
        result = xmlhttp.responseText;
    }
    return result;
}

function getRow(header, row) {
    var data = {};
    for (var i = 0; i < header.length; i++) {
        if(i === 1 || i === 2){
        }else if(i === 13){
            data["PrevProfession"] = row[header[i]];
        } else{
            data[header[i]] = row[header[i]];
        }
    }
    return data;
}

function updateSet(header, rows) {
    var set = [];
    for(var i = 0; i< rows.length; i++){
        set.push(getRow(header, rows[i]));
    }
    return set;
}

function averagePos(points){
    var long = 0;
    var lat  = 0;
    var num  = 0;
    for (var i = 0; i < points.length; i++) {
        long += parseFloat(points[i]["Longitude"]);
        lat  += parseFloat(points[i]["Latitude"]);
        num  ++;
    }
    return [long/num, lat/num];
}

function updateDisplay(rows, index){
    var data = rows[index];
    for(var i = 0; i < Object.keys(data).length; i++){
        var key = Object.keys(data)[i];
        document.getElementById(key).innerText = data[key];
    }
}

function prevOption(rows, index) {
    if(index > 0){
        index --;
        updateDisplay(rows, index);
    }
    return index;
}

function nextOption(rows, index) {
    if(index < rows.length - 1){
        index ++;
        updateDisplay(rows, index);
    }
    return index;
}